import React from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router';
import { BACKENDURL } from './Config.js';
import API from './Api';
import LoginPage from './LoginPage.jsx';
import RegisterPage from './RegisterPage.jsx';
import HomePage from './HomePage';
import UserPage from './UserPage';
import RoomPage from './RoomPage';
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import SessionRoom from './SessionRoom';

export const ApiContext = React.createContext();

export const AppTheme = createTheme({
  palette: {
    primary: {
      main: '#8C30F5',
    },
  },
  datePicker: {
    selectColor: '#8C30F5',
  },
});

function App() {
  const api = new API(BACKENDURL);
  return (
    <MuiThemeProvider theme={AppTheme}>
      <ApiContext.Provider value={api}>
        <BrowserRouter>
          <Switch>
            <Route path='/login'>
              <LoginPage/>
            </Route>
            <Route path='/register'>
              <RegisterPage/>
            </Route>
            <Route path='/home'>
              <HomePage/>
            </Route>
            <Route path='/user/:uid'>
              <UserPage/>
            </Route>
            <Route exact path='/room'>
              <RoomPage/>
            </Route>
            <Route path='/room/:sessionID'>
              <SessionRoom/>
            </Route>
            <Route path='/'>
              <Redirect to='/login'/>
            </Route>
          </Switch>
        </BrowserRouter>
    </ApiContext.Provider>
    </MuiThemeProvider>
  );
}

export default App;
