import pytest
from datetime import datetime
from src.error import InputError, AccessError
from src.session import sessionCreate, sessionInfo, sessionList, \
    sessionStart, sessionEnd, sessionJoin, sessionLeave
from src.auth import authRegister
from src.task import taskCreate, taskInvite
from src.helpers import clearData

#############  pytest fixtures  #############

@pytest.fixture
def example():
    """
    task1: Finish hackathon
           [user1]
    task2: Exercise
           [user1, user2]
    """
    clearData()

    user1 = authRegister("user1@gmail.com", "Password1", "fname1", "lname1")
    user2 = authRegister("user2@gmail.com", "Password2", "fname2", "lname2")

    dueDate = datetime(2021, 7, 28, 18, 30).timestamp()
    task1 = taskCreate(user1['token'], "Finish Hackathon", dueDate)['taskID']
    task2 = taskCreate(user1['token'], "Exercise", dueDate)['taskID']
    taskInvite(user1['token'], task2, user2['uID'])

    return {
        "u1Token" : user1['token'],
        "u1ID" : user1['uID'],
        "u2Token" : user2['token'],
        "u2ID" : user2['uID'],
        "task1" : task1,
        "task2" : task2
    }

@pytest.fixture
def date():
    return datetime(2067, 4, 16, 5, 30).timestamp()

@pytest.fixture
def sCreateInput(example, date):
    return {
        "user1" : {
                "token" : example['u1Token'],
                "taskID" : example['task1'],
                "duration" : 10,
                "scheduledTime" : date
            },
        "user2" : {
                "token" : example['u2Token'],
                "taskID" : example['task2'],
                "duration" : 20,
                "scheduledTime" : date
            }
    }


#############  helper functions  #############

def checkInputError(function, inputDic, key, invalidList):
    for value in invalidList:
        with pytest.raises(InputError):
            inputDic[key] = value
            function(**inputDic)

def checkAccessError(function, inputDic, key, invalidList):
    for value in invalidList:
        with pytest.raises(AccessError):
            inputDic[key] = value
            function(**inputDic)


#############  sessionCreate tests  #############

# successfully create multiple sessions from 1 task
def testCreateSessionSuccess(example, date):
    # test user1 making 3 sessions
    for i in range(3):
        session = sessionCreate(example['u1Token'], example['task1'], 60 + i, date)
        assert session == {'sessionID' : session['sessionID']}
        sessions = sessionList(example['u1Token'], example['task1'])['sessionsList']
        assert len(sessions) == 1 + i
        assert sessions[-1] == {
            "sessionID" : session['sessionID'],
            "isActive" : False,
            "duration" : 60 + i,
            "scheduledTime" : date,
            "startTime" : None,
            "endTime" : None,
            "secondsRemaining" : None,
            "reason" : None,
            "userIDs" : [example['u1ID']]
        }

    # test user2 making 5 sessions
    for i in range(5):
        session = sessionCreate(example['u2Token'], example['task2'], 14 + i, date)
        assert session == {'sessionID' : session['sessionID']}
        sessions = sessionList(example['u2Token'], example['task2'])['sessionsList']
        assert len(sessions) == 4 + i
        assert sessions[-1] == {
            "sessionID" : session['sessionID'],
            "isActive" : False,
            "duration" : 14 + i,
            "scheduledTime" : date,
            "startTime" : None,
            "endTime" : None,
            "secondsRemaining" : None,
            "reason" : None,
            "userIDs" : [example['u2ID']]
        }


# Input error with invalid task ID
def testSessionCreateInvalidTaskID(sCreateInput):
    invalidTaskIDs = [-50, -10000000000, -3, 0, None, \
        sCreateInput['user1']['taskID'] + sCreateInput['user2']['taskID'], \
        sCreateInput['user1']['taskID'] - sCreateInput['user2']['taskID'], \
        sCreateInput['user1']['taskID'] * sCreateInput['user2']['taskID']]
    checkInputError(sessionCreate, sCreateInput['user1'], "taskID", invalidTaskIDs)


# Input error with duration <= 0
def testSessionCreateInvalidDuration(sCreateInput):
    invalidDurations = [0, -30, -16, -60348, -1238, -8]
    checkInputError(sessionCreate, sCreateInput['user1'], "duration", invalidDurations)

# Access error with invalid token
def testSessionCreateInvalidToken(sCreateInput):
    invalidTokens = ["", "invalidtoken", "un lucky", "0"]
    checkAccessError(sessionCreate, sCreateInput['user1'], "token", invalidTokens)


# Access error when user is not part of the task
def testSessionCreateUsernotinTask(sCreateInput):
    checkAccessError(sessionCreate, sCreateInput['user1'], "token", [sCreateInput['user2']['token']])


#############  sessionDelete tests  #############

# successfully delete multiple sessions from 1 task


# successfully delete multiple session from multiple tasks


# Input error with invalid session ID


# Access error with invalid token


# Access error when user is not part of the session


#############  sessionInfo tests  #############

# successfully get session information


# Input error with invalid session ID


# Access error with invalid token


# Access error when user is not part of the session


#############  sessionList tests  #############

# successfully get list of sessions


# Input error with invalid task ID


# Access error with invalid token


# Access error when user is not part of the task



#############  sessionStart tests  #############

# successfully starts a session

# Input error with invalid session ID


# Input error when session is already active


# Access error with invalid token


# Access error when user is not part of the session


#############  sessionEnd tests  #############

# successfully ends a session


# Input error with invalid session ID


# Input error when session is already inactive


# Access error with invalid token


# Access error when user is not part of the session


#############  sessionJoin tests  #############

# successfully joins a valid session


# Input error with invalid session ID


# Input error when session is already active


# Access error with invalid token


# Access error when user is already part of the session


# Access error when user is not part of the task


#############  sessionLeave tests  #############

# successfully leaves a valid session


# Input error with invalid session ID


# Access error with invalid token


# Access error when user is not part of the session


#############  sessionInvite tests  #############

# successfully invites a valid user to a valid session


# Input error with invalid session ID


# Input error with invalid task ID


# Access error with invalid token


# Access error when inviter is ot already part of the session


# Access error when invitee is already part of the session


# Access error when invitee is not part of the task


