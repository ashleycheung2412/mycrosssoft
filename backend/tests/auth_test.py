from flask.globals import request
from src.error import InputError
from src.helpers import server_clear
import pytest
from src.auth import authLogin, authRegister, authLogout
from src.users import userProfile
from src.helpers import clearData
import requests


''' AUTH TESTS '''
def test_authRegister():

    clearData()
    user = authRegister(
        "admin@gmail.com",
        "admin123",
        "Admin",
        "Admin"
    )
    assert isinstance(user, dict) and isinstance(user['uID'], int)

def test_register_invalid_email():
    clearData()

    # Not an email test
    with pytest.raises(InputError):
        authRegister(
        "hey this aint a real email",
        "admin123",
        "Admin",
        "Admin"
    )

    # Taken email
    authRegister(
        "admin@gmail.com",
        "admin123",
        "Admin",
        "Admin"
    )
    with pytest.raises(InputError):
        authRegister(
        "admin@gmail.com",
        "admin123",
        "Admin",
        "Admin"
    )

def test_register_short_password():
    clearData()

    with pytest.raises(InputError):
        authRegister("admin@gmail.com", "pw", "Admin", "Admin")

def test_http_authRegister():
    server_clear()

    # Expected Outcome
    user = {
        'email' : "admin@gmail.com",
        'password' : "admin123",
        'firstName' : "Admin",
        'lastName' : "Admin"
    }
    r = requests.post("http://localhost:5052/auth/register" , json = user)
    user = r.json()
    assert user['uID'] == 1

    # Invalid Email
    user2 = {
        'email' : "This aint an email",
        'password' : "123abc!@#",
        'firstName' : "Foo",
        'lastName' : "Bar"
    }
    r = requests.post("http://localhost:5052/auth/register" , json = user2)
    user2Output = r.json()
    assert user2Output['message'] == "<p>aught_register: Invalid email<\p>"

    # In Use Email
    user3 = {
        'email' : "admin@gmail.com",
        'password' : "admin123",
        'firstName' : "Admin",
        'lastName' : "Admin"
    }
    r = request.post("http://localhost:5052/auth/register" , json = user3)
    user3Output = r.json()
    assert user3Output['message'] == "<p>aught_register: email admin@gmail.com already in use.<\p>"

    user4 = {
        'email' : "johnsmith@gmail.com",
        'password' : "p@ssword",
        'firstName' : "John",
        'lastName' : "Smith"
    }
    r = requests.post("http://localhost:5052/auth/register" , json = user4)
    user4Output = r.json()
    assert user4Output['message'] == "<p>authRegister: Password too short</p>"



def test_authLogin():

    clearData()
    user = authRegister(
        "admin@gmail.com",
        "admin123",
        "Admin",
        "Admin"
    )

    login = authLogin(
        "admin@gmail.com",
        "admin123")

    assert user == login

def test_http_authLogin():
    server_clear()

    user = {
        'email' : "admin@gmail.com",
        'password' : "admin123",
        'firstName' : "Admin",
        'lastName' : "Admin"
    }
    r = requests.post("http://localhost:5052/auth/register", json = user)

    # login the user
    user_input = {
        'email': "admin@gmail.com", 
        'password': "admin123", 
    }
    r = requests.post("http://localhost:5052/auth/login", json = user_input)
    user_input = r.json()
    assert user_input['uID'] == 1

''' USER TESTS '''
def test_user_profile():
    clearData()

    user = authRegister(
        "admin@gmail.com",
        "admin123",
        "Admin",
        "Admin"
    )

    user2 = authRegister(
        "johnsmith@gmail.com",
        "p@ssword",
        "John",
        "Smith"
    )

    outcome = userProfile(user['token'], user2['uID'])
    assert(outcome['uID'] == user2['uID'] and
            outcome['email'] == "johnsmith@gmail.com" and
            outcome['firstName'] == "John" and
            outcome['lastName'] == "Smith"
            )