"""
Functions regarding sessions
- sessionCreate
- sessionInfo
- sessionStart
- sessionEnd
- sessionJoin
- sessionLeave
"""
from datetime import datetime, timedelta, timezone
from threading import Timer
from src.error import InputError, AccessError
from src.helpers import getUniqueID, checkUserInTaskOrSession, getFieldFromFieldID
from src.data import data

###############################################################
#                                                             #
#                      SESSION FUNCTIONS                      #
#                                                             #
###############################################################

def sessionCreate(token, taskID, duration, scheduledTime):
    """
    Args:
        token (str)
        taskID (int): The ID of the task the session is created for
        duration (int): the session length in minutes
        scheduledTime (unix)

        #TODO: implement pomodoro
        {durationWork(int), durationBreak(int)}: the length in minutes for work and break

    Returns:
        sessionID (int)

    Possible Errors:
        InputError: invalid taskID
        AccessError: invalid token
        AccessError: user is not part of the task
    """
    # check duration is a positive amount
    if duration <= 0:
        raise InputError("duration must be greater than 0")

    # check token, user and task validity
    user, task = checkUserInTaskOrSession("task", taskID, token)

    # create new session and add to data
    sessionID = createSessionData(task, user['uID'], duration, scheduledTime)
    return {'sessionID' : sessionID}


def sessionDelete(token, sessionID):
    """
    Args:
        token (str)
        sessionID (int)

    Possible Errors:
        InputError: invalid sessionID
        AccessError: invalid token
        AccessError: user is not part of the session
    """
    # check token, user and session validity
    _, session = checkUserInTaskOrSession("session", sessionID, token)

    # delete session
    deleteSession(session)
    return


def sessionInfo(token, sessionID):
    """
    Args:
        token (str)
        sessionID (int)

    Returns:
        isActive (bool)
        scheduledTime (unix)
        duration (int)
        startTime (unix)
        endTime (unix)
        secondsRemaining (int)
        reason (str)
        users (users dic of uID, firstName, lastName)

    Possible Errors:
        InputError: invalid sessionID
        AccessError: invalid token
        AccessError: user is not part of the session
    """
    # check token, user and session validity
    _, session = checkUserInTaskOrSession("session", sessionID, token)

    # get the required info from the session
    sessionReturnInfo = createSessionInfo(session)

    return sessionReturnInfo


def sessionList(token, taskID):
    """
    Args:
        token (str)
        taskID (int)

    Returns:
        sessionsActive
        sessionsCompleted
        sessionsList
        (all are lists of dictionaries containing:)
            sessionID (int)
            isActive (bool)
            duration (int)
            scheduledTime(unix)
            startTime (unix)
            endTime (unix)
            reason (str)
            secondsRemaining (int)

    Possible Errors:
        InputError: invalid taskID
        AccessError: invalid token
        AccessError: user is not part of the task
    """
    # check token, user and task validity
    _, task = checkUserInTaskOrSession("task", taskID, token)
    sessionsActive, sessionsCompleted, sessionsList = createSessionsList(task['sessionIDs'])

    return {
        'sessionsActive' : sessionsActive,
        'sessionsCompleted' : sessionsCompleted,
        'sessionsList' : sessionsList
    }


def sessionStart(token, sessionID):
    """
    Args:
        token (str)
        sessionID (int)

    Returns:
        endTime (unix)

    Possible Errors:
        InputError: invalid sessionID
        InputError: session is already active
        AccessError: invalid token
        AccessError: user is not part of the session
    """
    # check token, user and session validity
    _, session = checkUserInTaskOrSession("session", sessionID, token)

    # set session to active and store start and end times
    endTime = activateSession(session)

    # at the end of the session duration set session to inactive
    duration = session['duration'] * 60
    timerEnd = Timer(duration, endSessionCheck, [sessionID])
    timerEnd.start()

    return {'endTime' : endTime}


def sessionEnd(token, sessionID, reason):
    """
    Args:
        token (str)
        sessionID (int)
        reason (str)

    Possible Errors:
        InputError: invalid sessionID
        InputError: session is already inactive
        AccessError: invalid token
        AccessError: user is not part of the session
    """
    # check token, user and session validity
    _, session = checkUserInTaskOrSession("session", sessionID, token)

    endSessionEarly(session, reason)
    return {}


def sessionJoin(token, sessionID):
    """
    Args:
        token (str)
        sessionID (int)

    Returns:
        endTime (unix)

    Possible Errors:
        InputError: invalid sessionID
        AccessError: invalid token
        AccessError: user is already part of the session
        AccessError: user is not part of the task
    """
    # check token, user, session and task validity
    session = getFieldFromFieldID("session", sessionID)
    user, _ = checkUserInTaskOrSession("task", session['taskID'], token)

    # if user is not already in session, add user to session
    joinSession(session, user['uID'])
    return {}


def sessionLeave(token, sessionID):
    """
    Args:
        token (str)
        sessionID (int)

    Returns:
        endTime (unix)

    Possible Errors:
        InputError: invalid sessionID
        AccessError: invalid token
        AccessError: user is not part of the session
    """
    # check token, user and session validity
    user, session = checkUserInTaskOrSession("session", sessionID, token)

    # remove user from session
    session['userIDs'].remove(user['uID'])
    return {}


def sessionInvite(token, sessionID, inviteeID):
    """
    Args:
        token (str)
        sessionID (int)
        inviteeID (int)

    Possible Errors:
        InputError: invalid sessionID
        InputError: invalid taskID
        AccessError: invalid token
        AccessError: inviter is not part of the session
        AccessError: invitee is already part of the session
        AccessError: invitee is not part of the task
    """
    # check token, user, session and session validity for inveter
    _, session = checkUserInTaskOrSession("session", sessionID, token)

    # check invitee is part of the task
    checkUserInTask(inviteeID, session['taskID'])

    # add user to session
    session['userIDs'].append(inviteeID)
    return {}


#TODO: make cleaner helper functions since importing from data
###############################################################
#                                                             #
#                  SESSION HELPER FUNCTIONS                   #
#                                                             #
###############################################################

def createSessionData(task, userID, duration, scheduledTime):
    """
    adds new session data to data dictionary and
    adds sessionID to corresponding task
    """
    # create new sessionID and add to corresponding task
    sessionID = getUniqueID("session")
    task['sessionIDs'].append(sessionID)
    # create new session info and add to data
    newSession = {
        "sessionID" : sessionID,
        "taskID" : task['taskID'],
        "userIDs" : [userID],
        "duration" : duration,
        "scheduledTime" : scheduledTime,
        "isActive" : False,
        "startTime" : None,
        "endTime" : None,
        "reason" : None
    }
    data['sessions'].append(newSession)

    return sessionID


def createSessionInfo(sessionData):
    """
    Creates the return dictionary for sessionInfo containing
    isActive, startTime, endTime, list of userIDs, reason
    """
    sessionReturn = {
        "isActive" : sessionData['isActive'],
        "duration" : sessionData['duration'],
        "scheduledTime" : sessionData['scheduledTime'],
        "startTime" : sessionData['startTime'],
        "endTime" : sessionData['endTime'],
        "reason" : sessionData['reason']
    }
    sessionReturn['users'] = getUsersList(sessionData['userIDs'])
    sessionReturn["secondsRemaining"] = calculateSecondsRemaining(sessionReturn)
    
    return sessionReturn


def createSessionsList(sessionIDsList):
    sessionsActive = []
    sessionsCompleted = []
    sessionsList = []

    for session in data['sessions']:
        # if session is in the task's sessions list
        if session['sessionID'] in sessionIDsList:
            # create session info dictinoary + sessionID
            sessionDetails = createSessionInfo(session)
            sessionDetails['sessionID'] = session['sessionID']

            # sort into the separate lists
            if sessionDetails['isActive']:
                sessionsActive.append(sessionDetails)
            elif sessionDetails['startTime']:
                sessionsCompleted.append(sessionsCompleted)
            else:
                sessionsList.append(sessionDetails)
    
    return sessionsActive, sessionsCompleted, sessionsList


def calculateSecondsRemaining(sessionReturnInfo):
    """
    If a session is active calculates the seconds left on the active session
    """
    secondsRemaining = 0
    if sessionReturnInfo['isActive']:
        currentTime = datetime.now(timezone.utc)
        endTime = datetime.fromtimestamp(sessionReturnInfo['endTime'], timezone.utc)
        timeDifference = endTime - currentTime
        secondsRemaining = timeDifference.total_seconds()

    return secondsRemaining


def getUsersList(userIDsList):
    """
    Given a list of user IDs, return a list of users where
    user is a dictionary of userID, firstName and lastName
    """
    usersList = []
    for user in data['users']:
        if user['uID'] in userIDsList:
            userInfo = {
                'uID' : user['uID'],
                'firstName' : user['firstName'],
                'lastName' : user['lastName']
            }
            usersList.append(userInfo)
    return usersList


def activateSession(session):
    """
    Check session is currently inactive then activates session,
    calculates start and endtime for the session
    """
    # check if session is already active
    if session['isActive'] or session['startTime']:
        raise InputError("activateSession: Session is already in progress or finished")

    #TODO: do some testing about timezone.utc
    # timeNow = datetime.now()
    # timeEnd = timeNow + timedelta(minutes=session['duration'])
    # get current time and end time when timer should end
    timeNow = datetime.now(timezone.utc)
    timeEnd = (timeNow + timedelta(minutes=session['duration'])).timestamp()

    session['isActive'] = True
    session['startTime'] = timeNow.timestamp()
    session['endTime'] = timeEnd

    return timeEnd


def endSessionCheck(sessionID):
    """ Check if session is already inactive, if not end the session """
    # check if session is inactive
    for session in data['sessions']:
        if session['sessionID'] == sessionID:
            # end session if currently active
            if session['isActive']:
                session['isActive'] = False
            return
    #TODO: does this need error handling if session cannot be found
    return


def endSessionEarly(session, reason):
    """
    Check if session is already inactive, if so raise InputError
    if not, end the session with updated endTime and reason added
    """
    # check if session is already inactive
    if not session['isActive']:
        raise InputError("Session is already inactive")
    
    timeNow = datetime.now(timezone.utc).timestamp()

    session['isActive'] = False
    session['endTime'] = timeNow
    session['reason'] = reason
    return


def joinSession(session, userID):
    """
    checks user is not already in session then adds user to session,
    if user was already in session raise AccessError
    """
    # check that user is not already in session
    if userID in session['userIDs']:
        raise AccessError("joinSession: User is already in session")

    # add user to the session
    session['userIDs'].append(userID)
    return


def deleteSession(session):
    """ deletes session from data and its corresponding task """
    # remove sessionID from corresponding task
    task = getFieldFromFieldID("task", session['taskID'])
    task['sessionIDs'].remove(session['sessionID'])

    # remove session from data
    data['sessions'].remove(session)
    return


def checkUserInTask(inviteeID, taskID):
    """ check user is in task, if not raise accessError """
    taskUserIDsList = getFieldFromFieldID("task", taskID)['userIDs']
    if inviteeID in taskUserIDsList:
        return
    
    raise AccessError("Invitee is not in task")
