import json
"""
data = {
    'users' : [],
        # "firstName" : str,
        # "lastName" : str,
        # "uID" : int,
        # "email" : str,
        # "password" : str,
        # "token" : {iat (int), exp (int), uID (int)}
    'tasks' : [],
        # "name" : str(name),
        # "dueDate": unixtimestamp,
        # "taskID" : int,
        # "userIDs" : [] int,
        # "sessionIDs" : [] int
    'sessions' : [],
        # "sessionID" : int
        # "taskID" : int
        # "userIDs" : [] int
        # "duration" : int
        # "scheduledTime" : unix timestamp
        # "isActive" : bool
        # "startTime" : unix timestamp
        # "endTime" : unix timestamp
        # "reason" : string
    'IDCounts' : {},
        # "user" : int,
        # "task" : int,
        # "session" : int,
}
"""

def loadData():
    with open("server_data.json", "r") as FILE:
        return json.load(FILE)

def storeData():
    with open("server_data.json", "w") as FILE:
        json.dump(data, FILE)

data = loadData()


