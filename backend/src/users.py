'''
    users.py: Handles profile related functions.
'''
from src.data import data
from src.error import InputError, AccessError
from src.helpers import getUserFromToken, getFieldFromFieldID

###############################################################
#                                                             #
#                       USER FUNCTIONS                        #
#                                                             #
###############################################################

def userProfile(token, uID):
    '''
    For a valid user, returns information about their ui, email, firstName,
    lastName.

    Parameters:
        token
        uID
    Returns:
        email
        firstName
        lastName
        tasks
    Exceptions:
        InputError(400):
            when user with uID is not valid.
    '''
    try:
        # check token validity
        getUserFromToken(token)

        # get user from uID and return dictionary of all user details except password
        user = getFieldFromFieldID("user", uID)
        return getUserProfileInfo(user)

    except AccessError as err:
        raise AccessError("userProfile: Calling user's token is invalid.") from err


def userSearch(token, email):
    '''
    Searchs for a user's userID by their email.

    Paramenter:
         token
         email
    Returns:
        userID
    Exceptions:
        InputError:
            invalid token
            invalid email
    '''
    # check token validity
    getUserFromToken(token)

    # return the userID of the user with the given email
    for user in data['users']:
        if user['email'] == email:
            return {'uID' : user['uID']}

    raise AccessError("userSearch: No user exists with that email")


###############################################################
#                                                             #
#                    USER HELPER FUNCTIONS                    #
#                                                             #
###############################################################

def getUserProfileInfo(user):
    '''
    Takes a user object and returns a copy wihtout senseitve data

    Parameters:
        user(dict): a user object, inlcuding personal information
    '''
    SENSITIVE_KEYS = ['password']
    userCopy = {}
    for key in user:
        if key not in SENSITIVE_KEYS:
            userCopy[key] = user[key]
    return userCopy
