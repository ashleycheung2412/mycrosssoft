from src.data import data
from src.error import InputError, AccessError
from src.helpers import getUserFromToken, getUniqueID, checkUserInTaskOrSession, \
                        getFieldFromFieldID


###############################################################
#                                                             #
#                       TASK FUNCTIONS                        #
#                                                             #
###############################################################

def taskList(token):
    """ returns a list of tasks which the user is part of """
    userID = getUserFromToken(token)['uID']
    tasksList = []

    # add tasks which user is part of
    for task in data["tasks"]:
        if userID in task["userIDs"]:
            tasksList.append(task)

    return {"tasks" : tasksList}


def taskCreate(token, name, dueDate):
    """ given token, name and dueDate, create a task and store it in data """
    userID = getUserFromToken(token)['uID']

    # create task and store in data
    taskID = createNewTask(userID, name, dueDate)

    return {"taskID" : taskID}


def taskInvite(token, taskID, uidAdd):
    """
    Given another user's ID and taskID, add invitee to task.
    Errors will occur if uidAdd is invalid or invitee is already part of task,
    or if user is not part of the task.
    """
    # check user and invitee validity
    checkUserInTaskOrSession("task", taskID, token)
    getFieldFromFieldID("user", uidAdd)

    # check task validity and add invitee to task
    task = getFieldFromFieldID("task", taskID)
    if uidAdd in task['userIDs']:
        raise AccessError("taskInvite: invitee is already part of the task")
    
    task['userIDs'].append(uidAdd)
    return {}


def taskUpdate(token, taskID, name, dueDate):

    uid = getUserFromToken(token)['uID']

    foundTask = False

    for task in data["tasks"]:
        if task["taskID"] == taskID:

            # checks the user calling the update
            # is actually part of the task
            if not task["userIDs"].count(uid):
                raise AccessError("This user is not allowed to change the \
                details of a task they are not a part of")
                
            # Updating the details of the task
            if name:
                task["name"] = name
            if dueDate:
                task["dueDate"] = dueDate
            foundTask = True
            break
    
    if not foundTask:
        raise InputError("taskID given is not a valid taskID")

    return {}


def taskDelete(token, taskID):
    """ Given a taskID and a user that is part of the task, delete the task """
    try:
        _, task = checkUserInTaskOrSession("task", taskID, token)
        data['tasks'].remove(task)
        isSuccess = True

    except InputError:
        isSuccess = False
    except AccessError:
        isSuccess = False

    return {"isSuccess" : isSuccess}


###############################################################
#                                                             #
#                    TASK HELPER FUNCTIONS                    #
#                                                             #
###############################################################

def createNewTask(userID, name, dueDate):
    """ Creates a new task dictionary and adds it to data """
    taskID = getUniqueID("task")

    newTask = {
        "name" : name,
        "dueDate": dueDate,
        "taskID" : taskID,
        "userIDs" : [userID],
        "sessionIDs" : [],
    }

    data['tasks'].append(newTask)
    return taskID
