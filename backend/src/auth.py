"""
auth.py: Handles authorization-related functions.

"""
import re
import jwt
import hashlib
from datetime import datetime, timedelta
from src.error import InputError, AccessError
from src.data import data
from src.helpers import getUserFromToken, getUniqueID

email_regex = re.compile("^[a-zA-Z0-9]+[\\._]?[a-zA-Z0-9]+[@]\\w+[.]\\w{2,3}$")
#24 random bytes from  os.urandom(), used for secure token signature
SECRET_KEY = b'\xa8\x9d`\xb0^F\xaf\x02lX$\x02z\xbc\xec\x9d\xf2\xb8\xa6P^\xf0c\xd8'


###############################################################
#                                                             #
#                       AUTH FUNCTIONS                        #
#                                                             #
###############################################################

def authRegister(email, password, firstName, lastName):
    '''
    authRegister: Given a users' first and last name, email address,
    and password, create a new account for them and return a new 
    'token' for that session. 

    Parameters:
        email (string) 
        password (string) 
        firstName (string)
        lastName (string)
    Exceptions:
        InputError(400):
            ▼ occurs when any of ▼
            -> invalid email
            -> email in use
            -> invalid password
            -> fistName invalid
            -> lastName invalid       
    Return Value:
        token
        uID
    '''
    # Check parameters
    checkRegisterParameters(email, password, firstName, lastName) 

    # create new user
    token, uID = createNewUser(email, password, firstName, lastName)

    return {
        "uID": uID,
        "token": token
    }


def authLogin(email, password):
    '''
    authLogin: Given a registered users' email and passowrd and
    returns a new 'token' for that session.

    Parameters:
        email (string)
        password (string)
    Exceptions:
        InputError(400):
            ▼ occurs when any of ▼  
            -> email invalid
            -> email not in use
            -> incorrect password
    Return:
        token
        uID
    '''
    # check that email and password are correct of a user
    user = checkUserEmail(email)
    checkPassword(user, password)

    # store decoded token in user data and return encoded token and uID
    decodedToken, encodedToken = generateUserToken(user['uID'])
    user['token'] = decodedToken

    return {"uID" : user['uID'], "token" : encodedToken}


''' auth logout '''
def authLogout(token):
    '''
    authLogout: Given an active token, invalidates the token to log the
    user out. If a valid token is given, and the user is successfully
    logged out, it return true, otherwise false.

    Parameters:
        token
    Exceptions:

    Return:
        is_success
    '''
    try:
        # if token is valid of a user, remove active token from user data
        user = getUserFromToken(token)
        user['token'] = None
        return {'is_success': True}
    except ValueError:
        return {'is_success': False}


###############################################################
#                                                             #
#                    AUTH HELPER FUNCTIONS                    #
#                                                             #
###############################################################

def generateUserToken(userID):
    '''
    generateUserToken: Generates authentication token for the user's session
    and stores it. 

    Parameters:
        userID
    Exceptions:
        InputError: 
            ▼ occurs when any of ▼  
            -> invalid userID
    Return:
        decodedToken
        encodedToken
    '''
    decodedToken = {  # https://realpython.com/token-based-authentication-with-flask/l
        "iat" : datetime.utcnow().timestamp(), #issued at
        "exp" : (datetime.utcnow() + timedelta(hours=48)).timestamp(), #expiry time= iat + 1 hour
        "uID" : userID
    }
    encodedToken = jwt.encode(decodedToken, SECRET_KEY, algorithm='HS256')

    return decodedToken, encodedToken


def checkRegisterParameters(email, password, firstName, lastName):
    """ Checks that all register inputs are valid """
    # Email Validity
    if email_regex.search(email) is None:
        raise InputError("authRegister: Invalid email")
    
    for user in data['users']:
        if user['email'] == email:
            raise InputError(f"authRegister: email {email} already in use.")

    # Password Security Check
    if len(password) < 6:
        raise InputError("authRegister: Password too short")

    # Name Parameters
    if len(firstName) < 1 or len(firstName) > 60 or len(lastName) < 1 or len(lastName) > 60:
        raise InputError("authRegister: Both first name and last name must be within 1-60 characters long inclusive")  


def createNewUser(email, password, firstName, lastName):
    """ creates new user dictionary and adds to data """
    # get uID, encoded password and decoded token to store in user
    uID = getUniqueID("user")
    encodedPassword = hashlib.sha256(password.encode()).hexdigest()
    decodedToken, encodedToken = generateUserToken(uID)

    # create user dictionary and store in data
    newUser = {
        "firstName" : firstName,
        "lastName" : lastName,
        "uID" : uID,
        "email" : email,
        "password" : encodedPassword,
        "token" : decodedToken
    }
    data['users'].append(newUser)

    return encodedToken, uID


def checkUserEmail(email):
    """ checks that email is a valid email address and is not already used by another user """
    # Check email is of a registered user
    if email_regex.search(email) is None:
        raise InputError("authLogin: Invalid email")

    #Find the user with the provided email, returns None if no user found
    user = next(filter(lambda u: u['email'] == email, data['users']), None)

    if not user:
        raise InputError(f"authLogin: No user with email {email} found.")

    return user


def encodePassword(password):
    """ takes in password string and returns an encoded password """
    return hashlib.sha256(password.encode()).hexdigest()


def checkPassword(user, password):
    """ encodes password and checks if it matches user's stored encoded password """
    encodedPassword = encodePassword(password)

    if user['password'] == encodedPassword:
        return
    
    raise InputError(f"authLogin: Incorrect password for user {user['email']}")

