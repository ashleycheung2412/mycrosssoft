import sys
from flask import Flask, abort, request
from flask_cors import CORS
from src.error import InputError, AccessError
import src.data as srcData
from json import dumps
from src import auth, helpers, task, session, users, data

# Initialise flask server
app = Flask(__name__)

"""
  Set up CORS headers
  Note: CORS is a security mechanism that browsers
  have that prevents a webpage from interacting with
  a server if they are on different domains. The following
  code basically allows our frontend webpage to communicate
  with this server without CORS blocking it
"""
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

#region ------ Helper functions
###############################################################
#                                                             #
#                      HELPER FUNCTIONS                       #
#                                                             #
###############################################################

def handle_error(func):
    def wrapper():
        try:
            func()
        except InputError as e_input:
            print(e_input.description)
            abort(400, e_input.description)
        except AccessError as e_access:
            print(e_input.description)
            abort(403, e_access.description)
    return wrapper

# Setup swagger api
helpers.setup_swagger_api(app)

@handle_error
@app.route('/')
def root():
  return 'This is the root page'

# Add other routes here

def saveReturn(responseObject):
    """
    save state prior to returning http response. the responseObject gets 
    passed through this function essentially and sent back again to caller.
    This is kind of a bypass function.
    """
    srcData.storeData()
    return dumps(responseObject)
#endregion

#region ------ Helper routes
###############################################################
#                                                             #
#                       HELPER ROUTES                         #
#                                                             #
###############################################################

@app.route("/clear", methods=['DELETE'])
def serverClear():
    helpers.clearData()
    srcData.storeData()
    return {}

#TODO: ONLY FOR DEBUGGING REMOVE LATER!!
@app.route("/data", methods=['GET'])
def showData():
    return saveReturn(data.data)

@app.route('/api/swagger.json')
def swagger_json():
  return send_from_directory('../api', 'swagger.json')

#endregion

#region ------ Routes for /auth/...
###############################################################
#                                                             #
#                         AUTH ROUTES                         #
#                                                             #
###############################################################

@handle_error
@app.route("/auth/login", methods=['POST'])
def authLoginRoute():
    inputdata = request.get_json()
    return saveReturn(auth.authLogin(inputdata['email'], inputdata['password']))

@handle_error
@app.route("/auth/register", methods=['POST'])
def authRegisterRoute():
    inputdata = request.get_json()
    return saveReturn(auth.authRegister(inputdata['email'], inputdata['password'], inputdata['firstName'], inputdata['lastName']))

@handle_error
@app.route("/auth/logout", methods=['POST'])
def authLogoutRoute():
    inputdata = request.get_json()
    return saveReturn(auth.authLogout(inputdata['token']))

#endregion

#region ---- Routes for /task/...
###############################################################
#                                                             #
#                         TASK ROUTES                         #
#                                                             #
###############################################################

@handle_error
@app.route('/task/list', methods=['GET'])
def getTaskList():
    token = request.args.get('token')

    listOfTasks = task.taskList(token)

    return saveReturn(listOfTasks)

@handle_error
@app.route('/task/create', methods=['POST'])
def createTask():
    payload = request.get_json()

    taskID = task.taskCreate(payload["token"], payload["name"], int(payload["dueDate"]))
    return saveReturn(taskID)

@handle_error
@app.route('/task/invite', methods=['POST'])
def inviteUserToTask():
    payload = request.get_json()

    task.taskInvite(payload["token"], int(payload["uidAdd"]), int(payload["taskID"]))
    return saveReturn({})

@handle_error
@app.route('/task/update', methods=['POST'])
def updateTask():
    payload = request.get_json()

    task.taskUpdate(payload["token"], int(payload["taskID"]), payload["name"], int(payload["dueDate"]))
    return saveReturn({})

@handle_error
@app.route('/task/delete', methods=['DELETE'])
def deleteTask():
    payload = request.get_json()
    return saveReturn(task.taskDelete(payload["token"], int(payload["taskID"])))

#endregion

#region ---- Routes for /task/session/...
###############################################################
#                                                             #
#                       SESSION ROUTES                        #
#                                                             #
###############################################################

@handle_error
@app.route("/task/session/create", methods=['POST'])
def taskSessionCreate():
    payload = request.get_json()
    return saveReturn(session.sessionCreate(payload['token'], int(payload['taskID']), \
                 int(payload['duration']), int(payload['scheduledTime'])))

@handle_error
@app.route("/task/session/delete", methods=['DELETE'])
def taskSessionDelete():
    payload = request.get_json()
    return saveReturn(session.sessionDelete(payload['token'], int(payload['sessionID'])))

@handle_error
@app.route('/task/session/info', methods=['GET'])
def taskSessionInfo():
    token = request.args.get('token')
    sessionID = int(request.args.get('sessionID'))
    return saveReturn(session.sessionInfo(token, sessionID))

@handle_error
@app.route('/task/session/list', methods=['GET'])
def taskSessionList():
    token = request.args.get('token')
    taskID = int(request.args.get('taskID'))
    return saveReturn(session.sessionList(token, taskID))

@handle_error
@app.route("/task/session/start", methods=['POST'])
def taskSessionStart():
    payload = request.get_json()
    return saveReturn(session.sessionStart(payload['token'], int(payload['sessionID'])))

@handle_error
@app.route("/task/session/end", methods=['POST'])
def taskSessionEnd():
    payload = request.get_json()
    return saveReturn(session.sessionEnd(payload['token'], int(payload['sessionID']), payload['reason']))

@handle_error
@app.route("/task/session/join", methods=['POST'])
def taskSessionJoin():
    payload = request.get_json()
    return saveReturn(session.sessionJoin(payload['token'], int(payload['sessionID'])))

@handle_error
@app.route("/task/session/leave", methods=['DELETE'])
def taskSessionLeave():
    payload = request.get_json()
    return saveReturn(session.sessionLeave(payload['token'], int(payload['sessionID'])))

@handle_error
@app.route("/task/session/invite", methods=['POST'])
def taskSessionInvite():
    payload = request.get_json()
    return saveReturn(session.sessionInvite(payload['token'], int(payload['sessionID']), int(payload['inviteeID'])))

#endregion

#region ---- Routes for /user/...
###############################################################
#                                                             #
#                         USER ROUTES                         #
#                                                             #
###############################################################

@handle_error
@app.route('/user/search', methods=['GET'])
def userSearchRoute():
    token = request.args.get('token')
    email = request.args.get('email')
    return saveReturn(users.userSearch(token, email))

#endregion


"""
  Run server in debug mode
  Default port used is 5052 but you can change it by passing
  in a command line argument.
  
  Example: Run server at port 8080
    python3 server.py 8080
"""
if __name__ == "__main__":
    app.run(port=(int(sys.argv[1]) if len(sys.argv) == 2 else 5052), debug=True)
