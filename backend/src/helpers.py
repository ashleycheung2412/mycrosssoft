"""
This contains all the helper functions of the project
"""
import json
import jwt
import json
import requests
from src.error import InputError, AccessError
from src.data import data
from flask_swagger_ui import get_swaggerui_blueprint


def clearData():
    data['users'].clear()
    data['tasks'].clear()
    data['sessions'].clear()
    data['IDCounts'].clear()
    return


def server_clear():
  requests.delete("http://localhost:5052/clear")


def decodeUserToken(token):
    '''
    decodeUserToken: Given a user's session token,
        return their user_id if their session is valid.

    Arguments:
        token(string): The user's signed token as a bytestring

    Exceptions:
        AccessError: occurs when the user's token is expired or they are not currently logged in
    Return Value:
        The user_id associated to the given session token (integer)
    '''
    secret = b'\xa8\x9d`\xb0^F\xaf\x02lX$\x02z\xbc\xec\x9d\xf2\xb8\xa6P^\xf0c\xd8'
    try:
        decodedToken = jwt.decode(token, secret, algorithms=['HS256'])
        return decodedToken
    except jwt.ExpiredSignatureError:
        raise AccessError(  # pylint: disable=W0707
            "decodeUserToken: Invalid token (session expired)")
    except jwt.InvalidTokenError as exc:
        raise AccessError("decodeUserToken: Invalid token (unknown). Please login again.") from exc


def getFieldFromFieldID(fieldName, fieldID):
    """ given a field name and field ID, return the field """
    if fieldName == "user":
        fieldIDName = "uID"
    else:
        fieldIDName = fieldName + "ID"

    for field in data[fieldName + 's']:
        if field[fieldIDName] == fieldID:
            return field
    
    raise InputError(f"getFieldFromFieldID: could not find {fieldName} with given {fieldIDName}")


def getUserFromToken(token):
    # decode token and get userID
    decodedToken = decodeUserToken(token)

    user = getFieldFromFieldID("user", decodedToken['uID'])
    if user['token'] == decodedToken:
        return user

    raise AccessError("getUserFromToken: Invalid token, did not match when decoded")


def getUniqueID(fieldName):
    """ returns a unique ID integer for the given field updates data """
    if not data['IDCounts']:
        data['IDCounts'] = {
            'user' : 100000,
            'task' : 100000,
            'session' : 100000,
        }

    uniqueID = data['IDCounts'][fieldName] + 1
    data['IDCounts'][fieldName] = uniqueID
    return uniqueID


def checkUserInTaskOrSession(field, fieldID, token):
    """
    Checks token validity and if user is in the task or session
    """
    #TODO: change to proper function
    # check token validity
    user = getUserFromToken(token)

    fieldName = field + "s"
    fieldIDName = field + "ID"

    # check fieldID validity and if user is in field
    for fieldInfo in data[fieldName]:
        if fieldInfo[fieldIDName] == fieldID:
            if user['uID'] in fieldInfo['userIDs']:
                return user, fieldInfo
            raise AccessError("User is not part of the " + field)
    raise InputError("Invalid " + fieldIDName)


def setup_swagger_api(app):
    """
    This sets up the swagger api for the server
    """
    # Set up swagger API
    SWAGGER_URL = '/api/docs'
    API_URL = '/api/swagger.json'
    # Call factory function to create our blueprint
    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
        API_URL,
        config={  # Swagger UI config overrides
            'app_name': "Documentation"
        },
    )
    app.register_blueprint(swaggerui_blueprint)


def jsonHTTPResponse(httpResponse):
    """
    Use this after calling
        - requests.post
        - request.get
        etc.
    
    It will convert the response to json through one function call.
    It also will alert the developer of HTTP error status_codes raised.
        - if this were not here, the console will say JSONDecodeError, when
            it tries to decode a HTTP error response.
        - This function raises an Error to display the error description.
    """
    debug = 1
    if (httpResponse.status_code == 400 or httpResponse.status_code == 403 and debug):
       raise InputError(f"HTTP Error with status_code {httpResponse.status_code}. \n \
         The error is: {httpResponse.text}")

    elif (httpResponse.status_code == 500 and debug):
        raise SystemError("500 SERVER ERROR")

    return json.loads(httpResponse.text)
