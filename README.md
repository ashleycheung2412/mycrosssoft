# MyCrossoft

## 1 Backend

The backend stub server is made with python and flask

### 1.1 Environment Setup (Linux)

This setup is for linux. You may set it up on windows but you will have to do your own research in how to do that.

  

For the sake of keeping track of all the modules that are being used in the backend server, make a virtual environment for python if possible. A virtual environment basically creates an isolated environment for a python project. Read more about setting up a virtual environment [here](https://realpython.com/python-virtual-environments-a-primer/)

  

1) Download the virtual environment package using python's package manager pip.

```
pip3 install virtualenv

```

2) Change the terminal to **\backend** directory

3) From there, create a new python virtual environment by typing the command below. This creates a folder called "env" which will store the python environment. Make sure you run the above command from inside the **\backend** directory.

```
python3 -m venv env

```

4) By default when you run the **python3** command in terminal, you are using the default python installed on the computer. However, we want use the python version that is created with the virtual environment. To do this we set the source of our python to the virtual environment one by running the following linux command. **Make sure to run this command inside the /backend directory**

```
source env/bin/activate

```

5) We now need to install the python modules that are already used such as flask. All the libraries that are already used are stored in the **requirements.txt** file. Hence we can tell pip to just install all the modules listed there. To do this, run the following command:

```
pip3 install -r requirements.txt

```

6) You have now set up your environment

### 1.2 Starting development server

To start the flask server, simply run the command from within the **/backend** directory:

```
python3 src/server.py

```

You can also choose the port number the server runs of by passing it as a command line argument. Example:

```
python3 src/server.py 8080

```

### 1.3 Additional Notes

1) Setting up a virtual environment isn't 100% necessary but keep track of all the python modules you install and place it in the requirements.txt file so that other people can also install it on their environment. If you are using a virtual environment, you can automatically tell pip to put all the installed python modules inside requirements.txt by running the command:

```
pip3 freeze > requirements.txt

```

## 2 API

The API docs are found on google docs by this [link](https://docs.google.com/document/d/1-WPd-DiBgzDuUhyN6sXb0QXHXTxsBAAmNQfPPo0cWNQ/edit?usp=sharing)

## 3 Frontend

To run the frontend first install yarn

Afterwards install the yarn dependencies for the frontend by going into the **/frontend** folder and running
```
yarn install
```

Now start the frontend by running
```
yarn start
```